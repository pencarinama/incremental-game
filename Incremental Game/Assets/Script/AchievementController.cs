﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Unity;

public class AchievementController : MonoBehaviour
{

    private static AchievementController _instance = null;
    public static AchievementController Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<AchievementController>();
            }
            return _instance;
        }
    }

    [SerializeField] private Transform _popupTransform;
    [SerializeField] private Text _popupText;
    [SerializeField] private float _popupShowDuration = 3f;
    [SerializeField] private List<AchievementData> _achievementList;
    private float _popupShowDurationCounter;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_popupShowDurationCounter > 0)
        {
            _popupShowDurationCounter -= Time.unscaledDeltaTime;
            _popupTransform.localScale = Vector3.LerpUnclamped(_popupTransform.localScale, Vector3.one, 0.5f);
        }
        else
        {
            _popupTransform.localScale = Vector2.LerpUnclamped(_popupTransform.localScale, Vector3.right, 0.5f);
        }
    }

    public void UnlockAchievement(AchievementType type, string value)
    {

        AchievementData achievement = _achievementList.Find((a) =>
        {
            if(a.isUnlocked)
            {
                return false;
            }
            if(a.Type == type)
            {
                if (type == AchievementType.TotalGold)
                {
                    return int.Parse(value) > int.Parse(a.Value);
                }
                return value == a.Value;
            }
            return false;
        });

        if(achievement != null && !achievement.isUnlocked)
        {
            achievement.isUnlocked = true;
            ShowPopupAchievement(achievement);
        }
    }

    private void ShowPopupAchievement(AchievementData achievement)
    {
        _popupText.text = achievement.Title;
        _popupShowDurationCounter = _popupShowDuration;
        _popupTransform.localScale = Vector2.right;
    }
}


[System.Serializable]
public class AchievementData
{
    public string Title;
    public AchievementType Type;
    public string Value;
    public bool isUnlocked;
}

public enum AchievementType
{
    UnlockResource,
    TotalGold
}
