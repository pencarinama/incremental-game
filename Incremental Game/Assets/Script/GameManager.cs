﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    [Range(0f, 1f)]
    public float AutoCollectPercentage = .1f;
    public ResourceConfig[] ResourceConfigs;

    public Transform ResourcesParent;
    public ResourceController ResourcePrefab;

    public Text GoldInfo;
    public Text AutoCollectInfo;

    private List<ResourceController> _activeResources = new List<ResourceController>();

    private float _collectSecond;
    public double TotalGold { get; private set; }

    public TapText TapTextPrefab;
    public Transform CoinIcon;
    private List<TapText> _tapTextPool = new List<TapText>();

    public Sprite[] ResourcesSprite;

    // Start is called before the first frame update
    void Start()
    {
        AddAllResources();
    }

    // Update is called once per frame
    void Update()
    {
        _collectSecond += Time.unscaledDeltaTime;
        if (_collectSecond >= 1f)
        {
            CollectPerSecond();
            _collectSecond = 0f;
        }

        CheckResourceCost();

        CoinIcon.transform.localScale = Vector3.LerpUnclamped(CoinIcon.transform.localScale, Vector3.one * 2f, 0.15f);
        CoinIcon.transform.Rotate(0f, 0f, Time.deltaTime * -100f);
    }

   private void AddAllResources()
    {
        bool showResource = true;
        foreach(ResourceConfig config in ResourceConfigs)
        {
            GameObject obj = Instantiate(ResourcePrefab.gameObject, ResourcesParent, false);
            ResourceController resource = obj.GetComponent<ResourceController>();
            resource.SetConfig(config);
            _activeResources.Add(resource);

            obj.gameObject.SetActive(showResource);
            if(showResource && !resource.IsUnlocked)
            {
                showResource = false;
            }
        }
    }

    private void CollectPerSecond()
    {
        double Output = 0;
        foreach(ResourceController resource in _activeResources)
        {
            if(resource.IsUnlocked)
            {
               Output += resource.GetOutput();
            }
        }

        Output *= AutoCollectPercentage;

        AutoCollectInfo.text = $"Auto Collect: { Output.ToString("F1")} / second";
        AddGold(Output);
    }

    public void AddGold(double value)
    {
        TotalGold += value;
        GoldInfo.text = $"Gold: { TotalGold.ToString("0")}";

        AchievementController.Instance.UnlockAchievement(AchievementType.TotalGold, TotalGold.ToString("0"));
    }

    public void CollectByTap(Vector3 tapPosition, Transform parent)
    {
        double Output = 0;

        foreach(ResourceController resource in _activeResources)
        {
            if(resource.IsUnlocked)
            {
                Output += resource.GetOutput();
            }
        }

        TapText tapText = GetOrCreateTapText();
        tapText.transform.SetParent(parent, false);
        tapText.transform.position = tapPosition;

        tapText.text.text = $"+{Output.ToString("0")}";

        tapText.gameObject.SetActive(true);
        CoinIcon.transform.localScale = Vector3.one * 1.75f;
        AddGold(Output);
    }

    private TapText GetOrCreateTapText()
    {
        TapText tapText = _tapTextPool.Find(t => !t.gameObject.activeSelf);
        if(tapText == null)
        {
            tapText = Instantiate(TapTextPrefab).GetComponent<TapText>();
            _tapTextPool.Add(tapText);
        }

        return tapText;
    }

    private void CheckResourceCost()
    {
        foreach(ResourceController resource in _activeResources)
        {
            bool isBuyable = false;
            if (resource.IsUnlocked)
            {
                isBuyable = TotalGold >= resource.GetUpgradeCost();
            }
            else
            {
                isBuyable = TotalGold >= resource.GetUnlockCost();
            }
            resource.ResourceImage.sprite = ResourcesSprite[isBuyable ? 1 : 0];
        }
    }

    public void ShowNextResource()
    {
        foreach(ResourceController resource in _activeResources)
        {
            if(!resource.gameObject.activeSelf)
            {
                resource.gameObject.SetActive(true);
                break;
            }
        }
    }
}

[System.Serializable]
public struct ResourceConfig
{
    public string Name;
    public double UnlockCost;
    public double UpgradeCost;
    public double Output;
}